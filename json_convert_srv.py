
########################################################################################
# Code Developed by CoreCompete 
# Name: json_convert_srv.py
# Version: 1
# Function: Host falcon service for JSON Converter and POST web service request to RTDM
# Date: 30-01-2018
# Updates
# 1.Handling Errors in Service Codes
# 2.Handling Errors in case RTDM unavailable
# 3.Hadnling Errors in Flatten Codes
# 4.Test GET request for this service
########################################################################################

import falcon
import json
import requests
import socket
import time
import datetime
from flatten import flatten_input_event
from normalize import normalize_output_event

class json_convert(object):

    def on_get(self, req, resp):

        """Handles GET requests"""
        try:
            resp.status = falcon.HTTP_200  # This is the default status
            resp.body = 'Hi this is JSON Parser, I am alive'
        except Exception as ex:
            resp.body = 'Hi this is JSON Parser, I am dead'

    def on_post(self, req, resp):

        """Handles POST requests"""
        try:
            def err_ret(chnl,er,err_nm):
                print(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))
                print('ERROR OCCURED IN JSON CONVERT SERVICE OF JSON PARSER - ',er)
                output = {"GenerateNewOffersResponse":{"OfferOutput":{"OutputNewOffer":{"OfferRequest":{"OfferEngineId":"XXXXX","OfferEngineVersion":"XXXXX",'Customer':{},'Offer':{}}},'Error':{"Code":chnl,"Message":err_nm}}}}
                return output

            #Receive the request JSON content sent from Fusion Service
            req_json_str = req.stream.read() 
            req_json = json.loads(req_json_str)

            #Parse the nested JSON and flatten the contents to RTDM acceptable format
            evtType, channel, flat_json = flatten_input_event(req_json)

            if evtType.upper() != 'ERROR':    #-- Capture Error in Flatten Code and revert back the error to Fusion without RTDM involvement

                #Build RTDM endpoint URL based on machine hostname when code deployed on Application Load Balancer
                mch_fqdn = socket.getfqdn()
                rtdm_resource = 'Event_NCE_Customer_Offer_RTDM_ATC'
                #rtdm_resource = 'Event_NCE_Customer_Offer_RTDM_ATC_Parser_CC'
                url = 'https://'+mch_fqdn+':8343/RTDM/rest/decisions/'+rtdm_resource

                headers = {'Content-Type':'application/json'}
                response = requests.post(url, verify=False, headers=headers, json=flat_json)
                rtdm_json_str = response.text 

                #Verify the Success of Request sent to RTDM
                if response.status_code == 201:

                    #Parse the flat JSON output from RTDM and normalize the contents to Fusion Acceptable Format
                    resp_json = normalize_output_event(json.loads(rtdm_json_str))
                    resp.status = falcon.HTTP_200
                else:
                    output = {}
                    print(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S'))
                    print('ERROR OCCURED IN CONNECTION BETWEEN JSON PARSER SERVICE AND RTDM SERVICE' )
                    output.update({"GenerateNewOffersResponse":{"OfferOutput":{"OutputNewOffer":{"OfferRequest":{"OfferEngineId":"XXXXX","OfferEngineVersion":"XXXXX",'Customer':{},'Offer':{}}},'Error':{"Code":str(response.status_code),"Message":"SAS RTDM Connection Error"}}}})
                    resp_json = json.dumps(output)
                    resp.status = falcon.HTTP_500

            else:
                resp_json = flat_json
                resp.status = falcon.HTTP_500

            #Send the response JSON content to Fusion Service
            resp.body = resp_json 
            
        except NameError as nm:
            output = err_ret('500',nm,'Parser Error')
            resp.status = falcon.HTTP_500
            resp.body = output 

        except ValueError as val:
            output = err_ret('400',val,'Parser - Input JSON Error')
            resp.status = falcon.HTTP_400
            resp.body = output 

        except TypeError as tp:
            output = err_ret('500',tp,'Parser Error')
            resp.status = falcon.HTTP_500
            resp.body = output 

        except AttributeError as ae:
            output = err_ret('400',ae,'Parser - Input JSON Error')
            resp.status = falcon.HTTP_400
            resp.body = output 

#Define the endpoint URL for Fusion Service to Send Requests
def json_convert_init(app):
    json_convert_srv = json_convert()
    app.add_route('/jsonconvert', json_convert_srv)

#main
# falcon.API instances are callable WSGI apps
app = falcon.API()
json_convert_init(app)